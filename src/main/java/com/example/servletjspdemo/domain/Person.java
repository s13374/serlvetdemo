package com.example.servletjspdemo.domain;

public class Person {
	
	private String firstName = "unknown";
	private int yob = 1900;
	private String surname;
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	private String email;
	private String confirmEmail;
	private String employerName;
	private String job;
	
	public Person() {
		super();
	}
	
	public Person(String firstName,String surname,String email,String confirmEmail,String employerName,String job, int yob) {
		super();
		this.firstName = firstName;
		this.surname = surname;
		this.email = email;
		this.confirmEmail = confirmEmail;
		this.employerName = employerName;
		this.job = job;
		
		
		this.yob = yob;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getYob() {
		return yob;
	}
	public void setYob(int yob) {
		this.yob = yob;
	}
}
